package code.api

import code.data.Product
import com.google.gson.Gson
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class Api {
    @GetMapping("/code/get")
    fun get(@RequestParam("id") id: Int): String {
        if (store[id]?.name != null) {
            return Gson().toJson(store[id])
        }
        return "Invalid id"
    }

    @GetMapping("/code/get-all")
    fun getAll(): String = Gson().toJson(store)

    @PostMapping("code/create")
    fun create(@RequestBody product: Product): Product {
        if (store[product.id]?.name == null) {
            product.id = ID++
            store[product.id!!] = product
            return product
        }
        return Product(null)
    }

    @PutMapping("code/update-product")
    fun update(@RequestBody product: Product): Product {
        if (store[product.id]?.name != null) {
            store[product.id!!] = product
            return product
        }
        return Product(null)
    }

    @PutMapping("code/update-image")
    fun addImage(@RequestParam("id") id: Int, @RequestBody image: ByteArray): Product {
        if (store[id]?.name != null) {
            store[id]!!.image = image
            return store[id]!!
        }
        return Product(null)
    }

    @DeleteMapping("code/delete")
    fun delete(@RequestParam("id") id: Int): String {
        store.remove(id)
        return "removed"
    }

    companion object LocalData {
        val store = mutableMapOf<Int, Product>()
        var ID = 1
    }
}
